export default function LogoCloud({ image, name }) {
  return (
    <img
      src={image}
      alt={name}
      title={name}
      className="h-48 w-64 object-cover object-center cursor-pointer rounded-sm"
    />
  )
}
