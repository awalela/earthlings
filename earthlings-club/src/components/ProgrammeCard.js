export default function ProgrammeCard({ image, title }) {
  return (
    <div className="w-72 max-w-xs relative rounded-md cursor-pointer shadow-lg mb-6">
      <img
        src={image.src}
        alt={image.title}
        className="rounded-md object-cover object-center h-72"
        style={{ backgroundColor: 'rgba(0, 0, 0, .99)' }}
      />
      <h3 className="text-xl absolute -mt-12 ml-2 text-white font-heading font-extrabold ">
        {title}
      </h3>
    </div>
  )
}
