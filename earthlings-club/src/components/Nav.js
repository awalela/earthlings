/* eslint-disable jsx-a11y/anchor-is-valid */
import { Fragment, useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Disclosure } from '@headlessui/react'
import { MenuIcon, XIcon } from '@heroicons/react/outline'
import { useHistory } from 'react-router-dom'

import fb from '../utils/fb'

import logo from '../assets/planet-earth.svg'

const navigation = ['About', 'Programmes', 'Cohorts', 'Gallery']

export default function Nav({ authenticated }) {
  let [activeIndex, setActiveIndex] = useState(null)
  let [user, setUser] = useState(null)
  let history = useHistory()

  useEffect(() => {
    let displayName = fb.auth().currentUser?.displayName
    console.log(displayName)
    setUser(displayName)
  }, [])

  const handleSignout = () => {
    fb.auth().signOut()
    history.push('/')
  }

  return (
    <div>
      <Disclosure as="nav" className="bg-green-800 max-w-full mx-auto">
        {({ open }) => (
          <>
            <div className="px-4 sm:px-6 lg:px-8">
              <div className="flex items-center justify-between h-16">
                <div className="flex items-center">
                  <Link to="/">
                    <div className="flex items-center gap-2 flex-shrink-0">
                      <img
                        className="h-12 w-12"
                        src={logo}
                        alt="Earthlings environment club"
                      />
                      <h4 className="font-heading text-gray-200 font-bold text-3xl">
                        earthlings
                      </h4>
                    </div>
                  </Link>

                  <div className="hidden md:block">
                    <div className="ml-10 flex items-baseline space-x-4">
                      {navigation.map((item, itemIdx) =>
                        itemIdx === activeIndex ? (
                          <Fragment key={item}>
                            {/* Current: "bg-green-900 text-white", Default: "text-gray-300 hover:bg-green-700 hover:text-white" */}
                            <a
                              href="#"
                              className="bg-green-900 text-white px-3 py-2 rounded-md text-sm font-medium"
                              onClick={() => setActiveIndex(itemIdx)}
                            >
                              {item}
                            </a>
                          </Fragment>
                        ) : (
                          <a
                            key={item}
                            href="#"
                            className="text-gray-300 hover:bg-green-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium"
                            onClick={() => setActiveIndex(itemIdx)}
                          >
                            {item}
                          </a>
                        ),
                      )}
                    </div>
                  </div>
                </div>
                <div className="hidden md:block">
                  <div className="ml-4 flex items-center md:ml-6">
                    <div className="flex items-center px-6 gap-4">
                      {user ? (
                        <>
                          {' '}
                          <div className="text-white text-lg font-semibold">
                            {user}
                          </div>
                          <button className="btn" onClick={handleSignout}>
                            Sign Out
                          </button>
                        </>
                      ) : (
                        <>
                          <Link to="/login" className="btn">
                            Sign In
                          </Link>
                          <Link to="/signup" className="btn">
                            Join Us!
                          </Link>
                        </>
                      )}
                    </div>
                  </div>
                </div>
                <div className="-mr-2 flex md:hidden">
                  {/* Mobile menu button */}
                  <Disclosure.Button className="bg-gray-800 inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                    <span className="sr-only">Open main menu</span>
                    {open ? (
                      <XIcon className="block h-6 w-6" aria-hidden="true" />
                    ) : (
                      <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                    )}
                  </Disclosure.Button>
                </div>
              </div>
            </div>

            <Disclosure.Panel className="md:hidden">
              <div className="pt-4 pb-3 border-t border-gray-700">
                <div className="flex flex-col px-6 gap-4">
                  <div className="text-white text-lg">
                    austinwalela@gmail.com
                  </div>
                  <Link to="/login" className="btn">
                    Sign In
                  </Link>
                  <Link to="/signup" className="btn">
                    Join Us!
                  </Link>
                </div>
              </div>
              <div className="px-6 pt-2 pb-6 space-y-1 sm:px-3">
                {navigation.map((item, itemIdx) =>
                  itemIdx === 0 ? (
                    <Fragment key={item}>
                      {/* Current: "bg-green-900 text-white", Default: "text-gray-300 hover:bg-green-700 hover:text-white" */}
                      <a
                        href="#"
                        className="bg-green-900 text-white block px-3 py-2 rounded-md text-base font-medium"
                      >
                        {item}
                      </a>
                    </Fragment>
                  ) : (
                    <a
                      key={item}
                      href="#"
                      className="text-gray-300 hover:bg-green-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
                    >
                      {item}
                    </a>
                  ),
                )}
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
    </div>
  )
}
