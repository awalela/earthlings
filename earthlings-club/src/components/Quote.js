/* eslint-disable jsx-a11y/alt-text */
export default function Quotes() {
  return (
    <div className="w-full bg-gray-200">
      <div className="max-w-6xl mx-auto flex gap-12 items-center">
        <img src="https://i.pinimg.com/474x/02/72/0b/02720b55beba1c2d570c781b83b6ff73.jpg" />
        <div className="text-gray-900 font-medium text-4xl font-heading">
          “We all share one planet and are one﻿ humanity; there is no escaping
          this reality.”
          <div className="text-gray-600 text-lg mt-4 font-sans">
            Wangari Maathai
          </div>
        </div>
      </div>
    </div>
  )
}
