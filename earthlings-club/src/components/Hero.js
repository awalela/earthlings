/* eslint-disable jsx-a11y/anchor-is-valid */
export default function Hero() {
  return (
    <div className="max-w-6xl mx-auto ">
      <h1 className="font-bold font-heading text-8xl text-yellow-300 mb-4 my-28">
        A good planet
      </h1>
      <h2 className="text-white font-heading text-8xl font-bold shadow tracking-wide mb-12">
        is hard to find
      </h2>
      {/* <a href="#" className="text-gray-100 text-lg font-medium">
        Learn how we're helping preserve our planet ->
      </a> */}
    </div>
  )
}
