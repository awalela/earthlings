import { Route, Redirect } from 'react-router-dom'

const PrivateRoute = ({ component: RouteComponent, authenticated, ...rest }) => {


  return (
    <Route
      {...rest}
      render={(routeProps) =>
        authenticated ? (
          <RouteComponent {...routeProps} />
        ) : (
          <Redirect
            to={{ pathname: '/login', state: { from: routeProps.location } }}
          />
        )
      }
    />
  )
}

export default PrivateRoute
