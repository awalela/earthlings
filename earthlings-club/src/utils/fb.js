import firebase from 'firebase'

let firebaseConfig = {
  apiKey: 'AIzaSyCubn4eFW_kMg-JUYj8vIf0B2oX2tbznO8',
  authDomain: 'earthlings-oia.firebaseapp.com',
  projectId: 'earthlings-oia',
  storageBucket: 'earthlings-oia.appspot.com',
  messagingSenderId: '701936601510',
  appId: '1:701936601510:web:eca9b971d5cf4b5cff2767',
}

// Initialize Firebase
const fb = firebase.initializeApp(firebaseConfig)

export default fb
