export const api = {
  programmes: [
    {
      image: {
        src:
          'https://images.unsplash.com/photo-1570965483111-44cfc35d457a?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80',
        title: 'Zanzibar Beach',
      },
      title: 'Zanzibar Space Tour',
    },
    {
      image: {
        src:
          'https://images.unsplash.com/photo-1500246432024-efe3c9116a56?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=864&q=80',
        title: 'Slave Ship',
      },
      title: 'Slave Trade Tour',
    },
    {
      image: {
        src:
          'https://images.unsplash.com/photo-1582113888274-e33535ddb25d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80',
        title: 'Mwalimu Julius Nyerere',
      },
      title: 'Mwalimu Nyerere Tour',
    },
    {
      image: {
        src:
          'https://images.unsplash.com/photo-1569510335991-1a6520e8c1b9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80',
        title: 'Slave Ship',
      },
      title: 'Kigali Remembrance Tour',
    },
  ],
  partners: [
    {
      logoImage:
        'https://secure.gravatar.com/avatar/05c8b6211222ad6dcc9323dc5afa2c5d?s=300&d=mm&r=g',
      name: 'Greenpeace Africa',
    },
    {
      logoImage:
        'https://www.sierraclub.org/sites/www.sierraclub.org/files/default_images/Large-Default-PullThru-03.png',
      name: 'Sierra Club',
    },
    {
      logoImage:
        'https://www.womenpeacesecurity.org/wp-content/uploads/logo_oxfam_featured.png',
      name: 'Oxfam',
    },
    {
      logoImage:
        'https://www-kiva-org.global.ssl.fastly.net/rgitceb5d0df9fc04ed7ca8a7901b5ae8d1064f89039/img/kiva_k_cutout_new.jpg',
      name: 'Kiva',
    },
  ],
  activities: [
    'Paragliding',
    'Ecological Tours',
    'Ooblek',
    'Yoga',
    'Vipassana',
    'Tree Planting',
    'Gardening',
    'Chess',
    "Children's Homes Visits",
  ],
}
