import { useEffect } from 'react'

import Nav from '../components/Nav'
import Hero from '../components/Hero'
import ProgrammeCard from '../components/ProgrammeCard'
import Quote from '../components/Quote'
import LogoCloud from '../components/LogoCloud'
import Footer from '../components/Footer'

import { api } from '../data/api'
import HeroImage from "../assets/hero-image.jpg"

function App() {
  useEffect(() => {
    localStorage.setItem('authState', false)
  }, [])

  return (
    <div
      className="w-full h-screen"
      style={{
        backgroundImage:  "url(" + HeroImage + ")",
        backgroundSize: 'cover',
        objectFit: 'cover',
      }}
    >
      <Nav />
      <Hero />
      <div className="max-w-6xl mx-auto mt-12">
        <h2 className="text-4xl text-white font-bold">
          Explore our opportunities
        </h2>
      </div>
      <div className="max-w-6xl mx-auto mt-6 mb-12 flex gap-6">
        {api.programmes.map((programme, index) => (
          <ProgrammeCard
            image={programme.image}
            key={index}
            title={programme.title}
          />
        ))}
      </div>
      <div className="max-w-6xl mx-auto">
        <div className="flex gap-6 mb-12">
          <div className="flex-1">
            <img
              src="https://images.unsplash.com/photo-1598335624134-5bceb5de202d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80"
              className="h-72 w-full object-cover object-center rounded-md"
              alt="Students planting trees"
            ></img>
          </div>

          <div className="mb-4 flex-1">
            <h2 className="mb-4 text-xl font-bold text-gray-900">About Us</h2>
            <p className="text-gray-700 mb-4">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem
              ipsum dolor sit amet consectetur adipiscing elit duis. Nisl
              suscipit adipiscing bibendum est ultricies integer quis auctor
              elit. Fermentum leo vel orci porta non pulvinar. Fermentum et
              sollicitudin ac orci phasellus egestas tellus rutrum tellus.
            </p>
            <button className="btn">View More</button>
          </div>
        </div>
        <div className="flex gap-6 mb-12">
          <div className="mb-4 flex-1">
            <h2 className="mb-4 text-xl font-bold text-gray-900">Programmes</h2>
            <p className="text-gray-700 mb-4">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem
              ipsum dolor sit amet consectetur adipiscing elit duis. Nisl
              suscipit adipiscing bibendum est ultricies integer quis auctor
              elit. Fermentum leo vel orci porta non pulvinar. Fermentum et
              sollicitudin ac orci phasellus egestas tellus rutrum tellus.
            </p>
            <button className="btn">View More</button>
          </div>
          <div className="flex-1">
            <img
              src="https://images.unsplash.com/photo-1586016413664-864c0dd76f53?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80"
              className="h-72 w-full object-cover object-center rounded-md"
              alt="Students planting trees"
            ></img>
          </div>
        </div>
        <div className="flex gap-6 mb-12">
          <div className="flex-1">
            <img
              src="https://images.unsplash.com/flagged/photo-1555861064-bafd3b04ce51?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=667&q=80"
              className="h-72 w-full object-cover object-center rounded-md"
              alt="Students planting trees"
            ></img>
          </div>

          <div className="mb-4 flex-1">
            <h2 className="mb-4 text-xl font-bold text-gray-900">About Us</h2>
            <p className="text-gray-700 mb-4">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem
              ipsum dolor sit amet consectetur adipiscing elit duis. Nisl
              suscipit adipiscing bibendum est ultricies integer quis auctor
              elit. Fermentum leo vel orci porta non pulvinar. Fermentum et
              sollicitudin ac orci phasellus egestas tellus rutrum tellus.
            </p>
            <button className="btn">View More</button>
          </div>
        </div>
      </div>
      <Quote />
      <div className="w-full bg-white flex flex-col gap-10 mt-4">
        <h2 className="text-gray-700 text-3xl text-center font-bold">
          Our Partners
        </h2>
        <div className="max-w-6xl mx-auto flex items-center gap-6 mb-8 flex-wrap">
          {api.partners.map((partner, idx) => (
            <LogoCloud image={partner.logoImage} key={idx} name={partner.name} />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default App
