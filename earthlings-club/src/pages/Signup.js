/* eslint-disable default-case */
/* eslint-disable no-undef */
import { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'

import fb from '../utils/fb'

import Nav from '../components/Nav'
import Footer from '../components/Footer'

import LoginImage from '../assets/login.jpg'

export default function Signup() {
  const [authEmailError, setAuthEmailError] = useState('')
  const [authPasswordError, setAuthPasswordError] = useState('')
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  let history = useHistory()

  const handleSignup = ({ firstName, lastName, email, password }) => {
    const userName = firstName + ' ' + lastName
    fb.auth()
      .createUserWithEmailAndPassword(email, password)
      .then((res) => {
        return res.user.updateProfile({
          displayName: userName,
        })
      })
      .then(() => {
        history.push('/login')
      })
      .catch((err) => {
        switch (err.code) {
          case 'auth/email-already-in-use':
          case 'auth/invalid-email':
            setAuthEmailError(err.message)
            break
          case 'auth/weak-password':
            setAuthPasswordError(err.message)
            break
        }
      })
  }

  return (
    <div className="w-full h-screen bg-gray-100">
      <Nav />
      <div className="flex min-h-screen bg-gray-50 dark:bg-gray-900">
        <div className="flex-1 h-full max-w-full overflow-hidden bg-white shadow">
          <div className="flex flex-col overflow-y-auto w-screen h-screen md:flex-row">
            <div className="flex-1 h-full">
              <img
                aria-hidden="true"
                className="object-cover object-center w-full h-full dark:hidden"
                src={LoginImage}
                alt="Abstract art"
              />
            </div>
            <main className="flex flex-1 items-center justify-center p-6 sm:p-12 md:w-1/2">
              <div className="w-full">
                <h1 className="mb-6 text-3xl font-bold text-gray-900">
                  Sign Up
                </h1>
                <form onSubmit={handleSubmit(handleSignup)}>
                  <div className="w-full mb-4 flex flex-col gap-4 justify-between lg:flex-row">
                    <label className="block">
                      <span className="text-gray-600 uppercase text-sm font-semibold tracking-wide">
                        First Name
                      </span>
                      <input
                        type="text"
                        className="my-1 block w-full"
                        placeholder="Mark"
                        {...register('firstName', { required: true })}
                      />
                      {errors.firstName && (
                        <div class="flex items-center gap-2">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20"
                            fill="#e02424"
                            class="w-4 h-4 inline flex-shrink-0 align-middle"
                          >
                            <path
                              fillRule="evenodd"
                              d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                              clipRule="evenodd"
                            />
                          </svg>
                          <span class="text-red-700 text-sm  align-middle ">
                            Please enter your first name
                          </span>
                        </div>
                      )}
                    </label>
                    <label className="block">
                      <span className="text-gray-600 uppercase text-sm font-semibold tracking-wide">
                        Last Name
                      </span>
                      <input
                        type="text"
                        className="my-1 block w-full"
                        placeholder="Twain"
                        {...register('lastName', { required: true })}
                      />
                      {errors.lastName && (
                        <div class="flex items-center gap-2">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20"
                            fill="#e02424"
                            class="w-4 h-4 inline flex-shrink-0 align-middle"
                          >
                            <path
                              fillRule="evenodd"
                              d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                              clipRule="evenodd"
                            />
                          </svg>
                          <span class="text-red-700 text-sm  align-middle ">
                            Please enter your last name
                          </span>
                        </div>
                      )}
                    </label>
                  </div>
                  <label className="block mb-4">
                    <span className="text-gray-600 uppercase text-sm font-semibold tracking-wide">
                      Email address
                    </span>
                    <input
                      type="email"
                      className="my-1 block w-full"
                      placeholder="marktwain@example.com"
                      {...register('email', { required: true })}
                    />
                    {errors.email && (
                      <div class="flex items-center gap-2">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="#e02424"
                          class="w-4 h-4 inline flex-shrink-0 align-middle"
                        >
                          <path
                            fillRule="evenodd"
                            d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                            clipRule="evenodd"
                          />
                        </svg>
                        <span class="text-red-700 text-sm  align-middle ">
                          Please enter your email address
                        </span>
                      </div>
                    )}

                    {authEmailError && (
                      <div class="flex items-center gap-2">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="#e02424"
                          class="w-4 h-4 inline flex-shrink-0 align-middle"
                        >
                          <path
                            fillRule="evenodd"
                            d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                            clipRule="evenodd"
                          />
                        </svg>
                        <span class="text-red-700 text-sm  align-middle ">
                          {authEmailError}
                        </span>
                      </div>
                    )}
                  </label>
                  <label className="block mb-6">
                    <span className="text-gray-600 uppercase text-sm font-semibold tracking-wide">
                      Password
                    </span>
                    <input
                      type="password"
                      className="my-1 block w-full"
                      placeholder="***********"
                      {...register('password', {
                        required: {
                          value: true,
                          message: 'Please enter a password',
                        },
                        minLength: {
                          value: 6,
                          message:
                            'Your password must be at least 6 characters long',
                        },
                      })}
                    />
                    {authPasswordError && (
                      <div class="flex items-center gap-2">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="#e02424"
                          class="w-4 h-4 inline flex-shrink-0 align-middle"
                        >
                          <path
                            fillRule="evenodd"
                            d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                            clipRule="evenodd"
                          />
                        </svg>
                        <span class="text-red-700 text-sm  align-middle ">
                          {authPasswordError}
                        </span>
                      </div>
                    )}
                    {errors.password && (
                      <div class="flex items-center gap-2">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="#e02424"
                          class="w-4 h-4 inline flex-shrink-0 align-middle"
                        >
                          <path
                            fillRule="evenodd"
                            d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                            clipRule="evenodd"
                          />
                        </svg>
                        <span class="text-red-700 text-sm  align-middle ">
                          {errors.password.message}
                        </span>
                      </div>
                    )}
                  </label>
                  <button type="submit" className="btn">
                    Sign Up
                  </button>
                </form>
                <p className="mt-4">
                  <Link
                    className="text-sm font-medium text-indigo-600 hover:underline"
                    to="/login"
                  >
                    Have an account already? Sign in instead.
                  </Link>
                </p>
              </div>
            </main>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  )
}
