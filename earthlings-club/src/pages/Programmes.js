import Nav from '../components/Nav'
import Footer from '../components/Footer'
import EarthVideo from '../assets/earth.mov'
import ProgrammesImage from '../assets/programmes.jpg'

import { api } from '../data/api'

export default function Programmes() {
  return (
    <div>
      <Nav />
      <div
        className="w-full h-96 relative"
        style={{
          backgroundImage: 'url(' + ProgrammesImage + ')',
          backgroundSize: 'contain',
          objectFit: 'cover',
        }}
      >
        <h2 className="absolute font-heading text-6xl font-bold text-white top-0 mt-44 left-0 ml-16">
          Our Programmes
        </h2>
        <video
          controls
          width="400"
          className="absolute mt-72 top-0 right-0 mr-24"
        >
          <source src={EarthVideo} type="video/mp4" />
          Sorry, your browser doesn't support embedded videos.
        </video>
      </div>
      <div className="max-w-6xl mx-auto my-12">
        <p className="w-2/5 text-gray-900">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur.
        </p>
      </div>

      <div className="w-full h-full">
        <div className="relative max-w-6xl mx-auto wrap overflow-hidden py-10 px-6 h-full">
          <div
            className="border-2-2 absolute max-w-6xl border-opacity-20 border-gray-700 h-full border"
            style={{ left: '37px' }}
          ></div>
          <div className="mb-8 flex gap-12 items-center w-full right-timeline">
            <div className="z-20 flex items-center order-1 bg-indigo-800 shadow-xl w-8 h-8 rounded-full">
              <h1 className="mx-auto font-semibold text-lg text-white">1</h1>
            </div>
            <div className="order-1 bg-gray-300 rounded-lg shadow w-11/12 px-6 py-6">
              <h3 className="mb-3 font-bold text-gray-800 text-xl">
                Lorem Ipsum
              </h3>
              <p className="text-sm leading-snug tracking-wide text-gray-900 text-opacity-100">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>
            </div>
          </div>
          <div className="mb-8 flex gap-12 items-center w-full right-timeline">
            <div className="z-20 flex items-center order-1 bg-indigo-800 shadow-xl w-8 h-8 rounded-full">
              <h1 className="mx-auto font-semibold text-lg text-white">2</h1>
            </div>
            <div className="order-1 bg-gray-300 rounded-lg shadow w-11/12 px-6 py-6">
              <h3 className="mb-3 font-bold text-gray-800 text-xl">
                Lorem Ipsum
              </h3>
              <p className="text-sm leading-snug tracking-wide text-gray-900 text-opacity-100">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>
            </div>
          </div>
          <div className="mb-8 flex gap-12 items-center w-full right-timeline">
            <div className="z-20 flex items-center order-1 bg-indigo-800 shadow-xl w-8 h-8 rounded-full">
              <h1 className="mx-auto font-semibold text-lg text-white">3</h1>
            </div>
            <div className="order-1 bg-gray-300 rounded-lg shadow w-11/12 px-6 py-6">
              <h3 className="mb-3 font-bold text-gray-800 text-xl">
                Lorem Ipsum
              </h3>
              <p className="text-sm leading-snug tracking-wide text-gray-900 text-opacity-100">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>
            </div>
          </div>
          <div className="mb-8 flex gap-12 items-center w-full right-timeline">
            <div className="z-20 flex items-center order-1 bg-indigo-800 shadow-xl w-8 h-8 rounded-full">
              <h1 className="mx-auto font-semibold text-lg text-white">4</h1>
            </div>
            <div className="order-1 bg-gray-300 rounded-lg shadow w-11/12 px-6 py-6">
              <h3 className="mb-3 font-bold text-gray-800 text-xl">
                Lorem Ipsum
              </h3>
              <p className="text-sm leading-snug tracking-wide text-gray-900 text-opacity-100">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="max-w-6xl mx-auto mt-24 mb-12">
        <h2 className="text-2xl font-bold text-gray-900 mb-6">
          Programme Activities
        </h2>
        <div className="flex justify-between flex-wrap">
          {api.activities.map((activity, idx) => (
            <div
              className="w-1/3 mb-3 flex items-center gap-2 text-gray-800"
              key={idx}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                viewBox="0 0 20 20"
                fill="rgb(55, 48, 163)"
              >
                <path
                  fillRule="evenodd"
                  d="M5 2a1 1 0 011 1v1h1a1 1 0 010 2H6v1a1 1 0 01-2 0V6H3a1 1 0 010-2h1V3a1 1 0 011-1zm0 10a1 1 0 011 1v1h1a1 1 0 110 2H6v1a1 1 0 11-2 0v-1H3a1 1 0 110-2h1v-1a1 1 0 011-1zM12 2a1 1 0 01.967.744L14.146 7.2 17.5 9.134a1 1 0 010 1.732l-3.354 1.935-1.18 4.455a1 1 0 01-1.933 0L9.854 12.8 6.5 10.866a1 1 0 010-1.732l3.354-1.935 1.18-4.455A1 1 0 0112 2z"
                  clipRule="evenodd"
                />
              </svg>
              {activity}
            </div>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  )
}
