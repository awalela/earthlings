/* eslint-disable default-case */
import { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import fb from '../utils/fb'

import Nav from '../components/Nav'
import Footer from '../components/Footer'

import LoginImage from "../assets/login.jpg"

export default function Login() {
  const [authEmailError, setAuthEmailError] = useState('')
  const [authPasswordError, setAuthPasswordError] = useState('')
  const { register, handleSubmit } = useForm()
  let history = useHistory()

  const onSubmit = ({ email, password }) => {
    fb.auth()
      .signInWithEmailAndPassword(email, password)
      .then(({ user }) => {
        console.log(user)
        // localStorage.setItem("user", JSON.parse(user))
        console.log('here')
        history.push('/programmes')
      })
      .catch((err) => {
        switch (err.code) {
          case 'auth/invalid-email':
          case 'auth/user-disabled':
          case 'auth/user-not-found':
            setAuthEmailError(err.message)
            break
          case 'auth/wrong-password':
            setAuthPasswordError(err.message)
            break
        }
      })
  }

  const onEmailFocus = () => setAuthEmailError('')
  const onPasswordFocus = () => setAuthPasswordError('')

  return (
    <div className="w-full h-screen bg-gray-100">
      <Nav />
      <div className="flex min-h-screen bg-gray-50 dark:bg-gray-900">
        <div className="flex-1 h-full max-w-full overflow-hidden bg-white shadow">
          <div className="flex flex-col overflow-y-auto w-screen h-screen md:flex-row">
            <div className="flex-1 h-full">
              <img
                aria-hidden="true"
                className="object-cover object-center w-full h-full dark:hidden"
                src={LoginImage}
                alt="Abstract art"
              />
            </div>
            <main className="flex flex-1 items-center justify-center p-6 sm:p-12 md:w-1/2">
              <div className="w-full">
                <h1 className="mb-6 text-3xl font-bold text-gray-900">Login</h1>
                <form onSubmit={handleSubmit(onSubmit)}>
                  <label className="block mb-4">
                    <span className="text-gray-600 uppercase text-sm font-semibold tracking-wide">
                      Email address
                    </span>
                    <input
                      type="email"
                      onFocus={onEmailFocus}
                      className="my-1 block w-full"
                      placeholder="austin@example.com"
                      {...register('email')}
                    />
                    {authEmailError && (
                      <div className="flex items-center gap-2">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="#e02424"
                          className="w-4 h-4 inline flex-shrink-0 align-middle"
                        >
                          <path
                            fillRule="evenodd"
                            d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                            clipRule="evenodd"
                          />
                        </svg>
                        <span className="text-red-700 text-sm  align-middle ">
                          {authEmailError}
                        </span>
                      </div>
                    )}
                  </label>
                  <label className="block mb-6">
                    <span className="text-gray-600 uppercase text-sm font-semibold tracking-wide">
                      Password
                    </span>
                    <input
                      type="password"
                      onFocus={onPasswordFocus}
                      className="my-1 block w-full"
                      placeholder="***********"
                      {...register('password')}
                    />
                    {authPasswordError && (
                      <div className="flex items-center gap-2">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="#e02424"
                          className="w-4 h-4 inline flex-shrink-0 align-middle"
                        >
                          <path
                            fillRule="evenodd"
                            d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                            clipRule="evenodd"
                          />
                        </svg>
                        <span className="text-red-700 text-sm  align-middle ">
                          {authPasswordError}
                        </span>
                      </div>
                    )}
                  </label>
                  <button type="submit" className="btn" to="/programmes">
                    Log in
                  </button>
                </form>
                <p className="mt-4">
                  <Link
                    className="text-sm font-medium text-indigo-600 hover:underline"
                    to="/login"
                  >
                    Forgot your password?
                  </Link>
                </p>
                <p className="mt-1">
                  <Link
                    className="text-sm font-medium text-indigo-600 hover:underline"
                    to="/signup"
                  >
                    Create account
                  </Link>
                </p>
              </div>
            </main>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  )
}
