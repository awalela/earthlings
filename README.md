# Earthlings Club #

Earthlings Club is a hypothetical environment club at Open Institute University.
This repository contains code for their website. It is my submission for 
an interview assignment for the role of Front-end Developer at 
The Open Institute. I thoroughly enjoyed myself building the app despite some
challenges. Have a look!

[https://earthlings-club.netlify.app](https://earthlings-club.netlify.app)

Here is a gif of the project in action (pretty large gif, may take a while to load):

[direct link to gif](https://bitbucket.org/awalela/earthlings/src/master/earthlings.gif)

![gif of project](https://bitbucket.org/awalela/earthlings/src/master/earthlings.gif)

## User Flow

The various components/page are pretty self-explanatory but the important thing to note is
that the Programmes page cannot be accessed directly from the navigation. A user has to sign up then log in before
they can view the programmes page.


## Stack

The website is a Single Page App built with [React](https://reactjs.org) and scaffolded
with good old [Create React App](https://create-react-app.dev/). Other tech stack choices include:

- [Tailwind CSS](https://tailwindcss.com/) for styling
- [React Router](https://reactrouter.com/) for SPA routing
- [React Hook Form](https://react-hook-form.com/) for Forms and Validation

and finally

- [Firebase](https://firebase.google.com) for user authentication

## Instructions

To run the project locally, clone this repo and `cd earthlings-club`. Then run `npm i && npm start`

## Challenges

There were a lot of little gotchas I encountered while building the app. If I had more time I would:

- Refactor component/styling architecture to be more modular 
- Use React Context to handle auth state
- Add Google OAuth in addition to email-based auth
- Manage route transitions more properly with animations/spinners
- Big one -- make the pages responsive for smaller screens. Currently the site only looks okay
on desktop
- Reduce image sizes for faster performance
- Write more comments in my code!



